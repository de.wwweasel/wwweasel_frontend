import Vue from 'vue'
import VueRouter from 'vue-router'

//import routes from './routes'

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */
import { gateway_url } from "src/assets/custom";
export default function ( { store } ) {

  const routes = [
    { path: '/', component: () => import('layouts/MainLayout.vue'),
      children: [
        { path: '', name:'home',component: () => import('src/pages/Home.vue') },
        { path: '/cv', name:'cv',component: () => import('pages/Curriculum.vue') },
        { path: '/app', name:'app',component: () => import('pages/App.vue') },
        { path: '/chat', name:'chat', component: () => import('src/pages/Person.vue'), 
          beforeEnter: (to, from, next ) => {
            if(store.state.oidcUser.authenticated===true){
              store.dispatch('chat/chatsByEmail');
              next()
            }else{
              //next({name: "Unauthorized"})
              window.location.href = gateway_url+"/oauth2/authorization/keycloak";
            }
          },
        },
        { path: '/unauthorized', name:'Unauthorized',component: () => import('pages/Unauthorized.vue') },
        { path: '/wwweasel', name:'wwweasel',component: () => import('pages/wwweasel.vue'),
          beforeEnter: async (to, from, next ) => {
            if(store.state.oidcUser.authenticated===true){
              if( store.state.oidcUser.oidcUser.roles.includes('WWWEASEL') ){
                next()
              }else{
                alert("Only Admins can access AdminUI !")
                from()
              }
            }else{
              //next({name: "Unauthorized"})
              window.location.href = gateway_url+"/oauth2/authorization/keycloak";
            }
          }    
        },
      ],
    },
    {
      path: "/:catchAll(.*)*",
      component: () => import('layouts/MainLayout.vue')
      //redirect: "/"
      //component: () => import("pages/Error404.vue"),
    },
  
    // Always leave this as last one,
    // but you can also remove it
    
    // {
    //   path: '*', redirect: '/'
    // },
    // {
    //   path: '*', 
    //   beforeEnter: async (to, from, next ) => {
    //     if(store.state.oidcUser.authenticated){
    //       next()
    //     }else{
    //       //next({name: "Unauthorized"})
    //       window.location.href = gateway_url+"/oauth2/authorization/keycloak";
    //     }
    //   },
    // }
  ]


  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  

  return Router
}
