export function setOidcUser ( state, oidcUser ) {
    state.oidcUser = oidcUser;
}
export function setAuthenticated ( state, value ) {
    state.authenticated = value;
}

export const setCsrftoken = (state, csrftoken) => {
    state.csrftoken = csrftoken
}
