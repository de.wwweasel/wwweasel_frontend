//import axios from 'axios';

export function setOidcUser ({commit}, oidcUser) {
    commit( 'setOidcUser', oidcUser );
}

export function setAuthenticated ({commit}, value) {
    commit( 'setAuthenticated', value );
}

export function setCsrftoken ({commit}, csrftoken) {
    commit( 'setCsrftoken', csrftoken );
}

import { gateway_url } from "src/assets/custom";

export async function requestOidcUser( {commit, state} ){


        try{
            // Thers's no need to send the CSRF Header because it will be sent as Cookie by the Browser anyway 
            const headers = new Headers({
                'X-XSRF-TOKEN': state.csrftoken
            });

            const response = await fetch(gateway_url+"/api/user", {
                method: "get",    
                //headers:headers,
            });
            /* 
                The response will always carry data, but you 
                have to make sure it carries a JSON, before commiting
                it to the store. The best way is to turn the response into
                text and then try to parse it to a JSON. If its not a JSON
                the function will fail an step into the catch clause. That 
                way no crappy response will end up in the store.
            */
            const text = await response.text();
            const data = JSON.parse(text);
            commit( 'setOidcUser', data );
            commit( 'setAuthenticated', true);
        }catch(error){
            console.log("You are not signed in.");
        }

}


// Spring wont see the csrf token with the following request...
export async function logoutjs( {commit, state} ){

    var formDataNew = new FormData();
    console.log(state.csrftoken);
    formDataNew.append("_csrf", state.csrftoken );

    const headers = new Headers({
        //'X-XSRF-TOKEN': state.csrftoken
        'Content-Type': 'application/x-www-form-urlencoded'
    });

    try {
        console.log("Iside logoutjs");
        const response = await fetch(gateway_url+"/logout", {
            method: "post",
            headers:headers,
            credentials: 'include',
            body: formDataNew,
        });
        if(response!==null){
            console.log("Inside response!");
            const data = await response.json();
            commit( 'setOidcUser', null );
            commit( 'setAuthenticated', false);
        }
        
    } catch (err) {
        console.error(err);
    }

}