export default function () {
  return {
    oidcUser: null,
    authenticated: false,
    csrftoken: null,
  }
}
