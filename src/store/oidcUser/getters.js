export async function getOidcUser ( state ) {
    return state.oidcUser;
}

export function getOidcUserSeq ( state ) {
    return state.oidcUser;
}

export function getAuthenticated ( state ) {
    return state.authenticated;
}

export function getCsrftoken ( state ) {
    return state.csrftoken;
}


