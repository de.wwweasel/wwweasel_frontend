//import state from "../tabs/state";

export const setChats = (state, chats) => {
    state.chats = chats
}

export const setCurrentOpenChat = (state, chatId) => {
    state.currentOpenChat = chatId
}

export const setCsrftoken = (state, csrftoken) => {
    state.csrftoken = csrftoken
}

export const setInitScroll = ( state, index ) => {
    state.initscrolls[index] = true;
}

export const setChatState = ( state, payload ) => {
    state.chatstates[payload.chatId] = {};
    state.chatstates[payload.chatId]['opened'] = payload.opened;
    state.chatstates[payload.chatId]['text'] = payload.text;
}

export const setChatText = ( state, text ) => {
    state.chattext = text;
    state.chatstates[payload.chatId]['opened'] = payload.opened;
    state.chatstates[payload.chatId]['text'] = payload.text;
}
