export async function getChats ( state ) {
    return state.chats;
}

export function getCurrentOpenChat ( state ) {
    return state.currentOpenChat;
}

export function getCsrftoken ( state ) {
    return state.csrftoken;
}