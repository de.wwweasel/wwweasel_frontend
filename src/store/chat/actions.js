//import axios from 'axios';

export function setPersonWebDTO ({commit}, personWebDTO) {
    commit( 'setPersonWebDTO', personWebDTO );
}

export function setCurrentOpenChat ({commit}, chatId) {
    commit( 'setCurrentOpenChat', chatId );
}

export function setCsrftoken ({commit}, csrftoken) {
    commit( 'setCsrftoken', csrftoken );
}

export function setInitScroll ({commit}, index) {
    commit( 'setInitScroll', index );
}

export function setChatState ({commit}, payload) {
    commit( 'setChatState', payload );
}

export function setChatText ({commit}, text) {
    commit( 'setChatText', text );
}

import { gateway_url } from "src/assets/custom";

export async function chatsByEmail( {commit, state}){
        
        try{
            const response = await fetch(gateway_url+"/api/chat/chatsByEmail", {
                method: "get",    
                //headers:headers,
            });
            /* 
                The response will always carry data, but you 
                have to make sure it carries a JSON, before commiting
                it to the store. The best way is to turn the response into
                text and then try to parse it to a JSON. If its not a JSON
                the function will fail an step into the catch clause. That 
                way no crappy response will end up in the store.
            */
            const text = await response.text();
            const data = JSON.parse(text);
            // data is an array of Chatrooms, if request worked
            // if it didnt work its an Obbject with status code, etc which you dont wanna store
            if(data.length>0){
                commit( 'setChats', data );
            }
        }catch(errror){
            console.log("Could not request chats.");
        }

}

export async function postMessage( {commit, state}, messageDTO ){

    try {
        
        const headers = new Headers({
            'X-XSRF-TOKEN': state.csrftoken,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        
        const response = await fetch(gateway_url+"/api/chat/message", {
            method: "post",    
            headers:headers,
            body: JSON.stringify(messageDTO),
        });
        const text = await response.text();
        const data = JSON.parse(text);

    } catch (err) {
        console.error(err);
    }


}

